#!/usr/bin/env sh

#アプリ名
app_name=$(node -pe 'require("./package.json").name')
echo 'app_name='$app_name

now rm $app_name -y

# 公開
now --public

#公開時のURL
url=$(now ls $app_name | grep $app_name | awk '{{print $2}}')
echo 'url='$url

#URLにアンダースコアは使えないのでハイフンに置き換え
app_name2=$(echo $app_name | sed -e 's/_/-/g')
echo 'app_name2='$app_name2

#URLにprefixをつける
app_name3='f01-'$app_name2
echo 'app_name3='$app_name3

#alias設定
now alias $url $app_name3

#※aliasは早いものがちのため
#　Success!が表示されることを確認すること

#公開URL
echo 'https://'${app_name3}'.now.sh/'
