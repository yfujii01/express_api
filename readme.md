# 認証用API

- 利用アプリ

https://gitlab.com/yfujii01/nuxt_login_sample2

- 動作確認

npm install

npm run dev

curl -X POST http://localhost:3001/auth -H "Content-type: application/json" -d '{"email":"demo","pass":"pass"}'

返却値
```
{"result":"true"}
```

## デプロイ

- deployコマンド

npm run deploy

- 公開URL
$ echo 'https://'${app_name3}'.now.sh/'

- 動作確認1

https://expressapi-wqbaawixez.now.sh/

- 動作確認2

curl -X POST https://expressapi-wqbaawixez.now.sh/auth -H "Content-type: application/json" -d '{"email":"demo","pass":"pass"}'
