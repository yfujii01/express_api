const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.get('/', (req, res) => res.send('Hello World!'));

// CORSを許可する
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.post('/', (req, res) => {
    res.send('Hello World!')
});

//curl -X POST http://localhost:3001/auth -H "Content-type: application/json" -d '{"email":"demo","pass":"pass"}'
app.post('/auth', (req, res) => {
    const body = req.body;
    console.log(body);
    console.log(body.email);
    console.log(body.pass);

    let result = {};
    if (body.email === 'demo' && body.pass === 'pass') {
        result.result = "true";
    } else {
        result.result = "false";
    }
    res.json(result);
});


app.listen(3001, () => console.log('Example app listening on port 3001!'));
